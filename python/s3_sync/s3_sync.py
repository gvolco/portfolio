import argparse
import ConfigParser
import os.path
from sys import exit
import pyinotify
import re
import boto3
import logging

def s3_upload(path,name):
    logging.warning("Uploadig " + name)
    try:
        with open(path, 'rb') as file_handle:
            s3_client.put_object(ACL='private',Body=file_handle,Bucket=s3_bucket,Key=name)
    except Exception as e:
        logging.warning("Could not upload " + name + ": " + str(e))

def filter_filename(event):
    if regex_filter and not re.match(regex_filter,event.name):
        return True #do nothing
    else:
        return False #upload file

class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self, event):
        s3_upload(event.pathname,event.name)
    def process_IN_MODIFY(self, event):
        s3_upload(event.pathname,event.name)
    def process_IN_ATTRIB(self, event):
        s3_upload(event.pathname,event.name)


parser = argparse.ArgumentParser()
parser.add_argument("--config","-c", help="Specify the config file path. Default ./s3_sync.conf", default="s3_sync.conf")
args = parser.parse_args()

if not os.path.isfile(args.config):
    logging.warning("Config file not found: " + args.config)
    exit(1)

config = ConfigParser.ConfigParser({'RegexFilter': None})
config.read(args.config)
log_file=config.get("Paths", "LogFile")
source_folder=config.get("Paths", "SourceFolder")
regex_filter=config.get("Paths", "RegexFilter")
s3_bucket=config.get("AWS", "S3Bucket")
aws_region=config.get("AWS", "Region")

logging.basicConfig(filename=log_file, filemode='a',format='%(asctime)s: %(message)s')
logging.warning("Starting s3_config daemon")

s3_client = boto3.client('s3',region_name=aws_region)

wm = pyinotify.WatchManager()
mask = pyinotify.IN_DELETE | pyinotify.IN_MODIFY | pyinotify.IN_ATTRIB

notifier = pyinotify.Notifier(wm, EventHandler(pevent=filter_filename))
wdd = wm.add_watch(source_folder, mask, rec=True)

notifier.loop()
