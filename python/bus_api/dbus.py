import requests
from haversine import haversine

class Dbus:

    api_endpoint="https://data.dublinked.ie/cgi-bin/rtpi/busstopinformation"

    def __init__(self):
        self.stop_list=[]
        try:
            api_response=requests.get(Dbus.api_endpoint)
            for stop in api_response.json()['results']:
                self.stop_list.append({'stopid': stop['stopid'],'latitude': float(stop['latitude']),'longitude': float(stop['longitude'])})
        except Exception as e:
            print "Error while requesting data to the API: " + str(e)

    def closest_stop_list(self,latitude,longitude):
        for i in range(len(self.stop_list)):
            distance=haversine((latitude,longitude),(self.stop_list[i]['latitude'],self.stop_list[i]['longitude']))
            self.stop_list[i]['distance']=distance
        self.stop_list.sort(key=lambda stop: stop['distance'])
        return(self.stop_list[:10])

    def is_status_ok(self):
        if len(self.stop_list) > 0:
            return True
        else:
            return False
