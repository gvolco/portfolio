from flask import Flask, request, jsonify
from dbus import Dbus

app = Flask(__name__)
dbus_client = Dbus()

@app.route('/v1/closeststops',methods=['GET'])
def index():
    try:
        latitude=float(request.args.get('lat'))
        longitude=float(request.args.get('lon'))
    except Exception as e:
        return 'Invalid parameters: ' + str(e), 500
    return jsonify(dbus_client.closest_stop_list(latitude,longitude))

@app.route('/v1/status',methods=['GET'])
def status():
    if dbus_client.is_status_ok():
        return 'OK'
    else:
        return 'FAIL', 500

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
