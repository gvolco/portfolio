# Introduction

In this repo you will find the following sample projects:

* CI
* bash_scripts/s3_sync_cron.sh
* chef/node-app
* chef/tomcat-app
* cloudformation/autoscaling-elb-vpc.json
* cloudformation/single-ec2-vpc.json
* python/bus_api
* python/s3_sync

Each one of them is described below, including the steps required to run and test them.

# CI

A Continuous Integration pipeline built on top of Jenkins that performs the following tasks:

* Retrieves a [test Java project](https://bitbucket.org/gvolco/java-test-app/)
* Builds the project using Maven and runs Unit tests
* Generates coverage reports
* Analyzes the code using SonarQube, importing test results and coverage reports.

### How to test it using Docker Compose
Make sure you have [Docker Compose](https://docs.docker.com/compose/install/#install-compose) installed before starting.
```
cd CI
docker-compose build
docker-compose up
```
After starting the containers, you can test the pipeline by browsing:

* http://127.0.0.1:8080/job/java-test-app-CI/ (user: admin / pass: ChangeMe)

Once the job is executed, Sonar will produce a report on the following URL:

* http://127.0.0.1:9000/dashboard/index/com.gvolco:java-test-app

It might take a few moments until Sonar analysis is completed.

# bash_scripts/s3_sync_cron.sh

A bash script used to traverse a specific directory structure, syncing them to S3.

### Usage
```
bash_scripts/s3_sync_cron.sh hourly
bash_scripts/s3_sync_cron.sh daily
```

# chef/node-app

A chef cookbook that performs the following tasks:

* Installs nodejs
* Installs mongodb
* Deploys a sample node application, installing the required npm dependencies
* Configures a startup script for the node app

Additionally the folder test/integration contains a few integration tests.

### How to test it with Vagrant
Before starting install [Vagrant](https://www.vagrantup.com/downloads.html)

Additionally you will need the Berkshelf plugin for Vagrant:
```
vagrant plugin install vagrant-berkshelf
```
Then you can deploy the cookbook by running:
```
cd chef/node-app
vagrant up
curl http://127.0.0.1:3000 # you can use either curl or a browser
vagrant destroy
```

### How to run integration tests
Before starting install [Vagrant](https://www.vagrantup.com/downloads.html) and the [Chef developer kit](https://downloads.chef.io/chefdk)

Then you can run the integration tests:
```
cd chef/node-app
kitchen converge
kitchen verify  # runs integration tests
# you can also browse http://127.0.0.1:3000
kitchen destroy
```

# chef/tomcat-app

A chef cookbook that performs the following tasks:

* Installs Java runtime
* Installs Tomcat
* Deploys a sample WAR application
* Generates a self signed SSL certificate
* Installs and configures nginx acting as a reverse proxy, with SSL termination.

Additionally the folder test/integration contains a few integration tests.

### How to test it with Vagrant
Before starting install [Vagrant](https://www.vagrantup.com/downloads.html)

Additionally you will need the Berkshelf plugin for Vagrant:
```
vagrant plugin install vagrant-berkshelf
```
Then you can deploy the cookbook by running:
```
cd chef/tomcat-app
vagrant up
curl -skL http://127.0.0.1:9080/ # you can use either curl or a browser
vagrant destroy
```

### How to run integration tests
Before starting install [Vagrant](https://www.vagrantup.com/downloads.html) and the [Chef developer kit](https://downloads.chef.io/chefdk)

Then you can run the integration tests:
```
cd chef/tomcat-app
kitchen converge
kitchen verify  # runs integration tests
# you can also browse http://127.0.0.1:9080
kitchen destroy
```

# cloudformation/autoscaling-elb-vpc.json

Cloudformation template that creates the following resources:

* A VPC with 2 public subnets and 2 private subnets.
* A NAT GW for the private subnets, Network ACLs and Sec Groups.
* A public ELB and an Autosclaing group (on the private subnets).
* Instance setup driven by cfn-init, and resource signaling using cfn-signal.

# cloudformation/single-ec2-vpc.json

Cloudformation template that creates the following resources:

* A VPC with a public subnet.
* Network ACLs and Sec Groups.
* A single EC2 instance that bootstraps Chef, and deploys the **tomcat-app** cookbook.
* The CF parameters httpPort and httpsPort are injected into the cookbook, to set up the underlying services.
* Instance setup driven by cfn-init, and resource signaling using cfn-signal.

The chef cookbook is deployed in 'solo' mode (without a chef server). This setup could be simplified by using a chef server.

# python/bus_api

A simple Python REST API implemented with Flask.

Given a latitude and a longitude, the API returns a JSON array with the closest bus stops on Dublin city.

### How to run it from the console:
```
cd python/bus_api
pip install -r conf/requirements.txt
python2.7 api.py

curl http://127.0.0.1:5000/v1/status
curl "http://127.0.0.1:5000/v1/closeststops?lat=53.310537&lon=-6.280893"
```

### How to run it with Docker:
```
cd python/bus_api
docker build -t gvolco/bus_api .
docker run -d -p 8080:80 gvolco/bus_api

curl http://127.0.0.1:8080/v1/status
curl "http://127.0.0.1:8080/v1/closeststops?lat=53.310537&lon=-6.280893"
```
Note that the Docker version uses **nginx** and **uwsgi**.

# python/s3_sync

A Python daemon that can be used to sync in real time a given directory to S3.
This daemon relies on the kernel inotify feature, implemented through [pyinotify](https://github.com/seb-m/pyinotify)

### Running manually from the console:
```
cd python/s3_sync
pip install -r requirements.txt
python2.7 s3_sync.py -c s3_sync.conf
```

Before running, make sure modify the config file s3_sync.conf to suit your needs.
Additionally you will need an AWS instance profile providing permissions for the action s3:PutObject*
