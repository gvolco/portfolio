#!/usr/bin/env bats

@test "Check sessions collection" {
  mongo 127.0.0.1/noobjs_dev --eval 'db.getCollectionNames()' --quiet | grep 'sessions'
}

@test "Port 3000 listening" {
  netstat -plan | grep LISTEN | grep 3000
}

@test "Curl returns nodejs-express-mongoose-demo" {
  curl -s http://127.0.0.1:3000/ | grep nodejs-express-mongoose-demo
}
