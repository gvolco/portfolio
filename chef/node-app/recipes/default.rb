#
# Cookbook:: node-app
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe "nodejs"
include_recipe "sc-mongodb"

poise_service_user node['nodeapp']['app_user'] do
  home node['nodeapp']['app_dir']
end

directory node['nodeapp']['app_dir'] do
  owner node['nodeapp']['app_user']
  group node['nodeapp']['app_user']
  mode '0755'
  action :create
end

git node['nodeapp']['app_dir'] do
  repository 'git://github.com/madhums/node-express-mongoose-demo.git'
  revision 'master'
  user node['nodeapp']['app_user']
  group node['nodeapp']['app_user']
  action :sync
end

nodejs_npm "npm install" do
  path node['nodeapp']['app_dir']
  user node['nodeapp']['app_user']
  group node['nodeapp']['app_user']
  json true
end

remote_file "Copy env file" do
  path "#{node['nodeapp']['app_dir']}/.env"
  source "file://#{node['nodeapp']['app_dir']}/.env.example"
  owner 'root'
  group 'root'
  mode 0755
end

poise_service 'nodeapp' do
  command '/usr/bin/npm start'
  user node['nodeapp']['app_user']
  directory node['nodeapp']['app_dir']
end
