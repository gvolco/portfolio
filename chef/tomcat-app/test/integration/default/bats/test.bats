#!/usr/bin/env bats

@test "Port 8080 listening" {
  netstat -plan | grep LISTEN | grep 8080
}

@test "Port 9080 listening" {
  netstat -plan | grep LISTEN | grep 9080
}

@test "Port 9443 listening" {
  netstat -plan | grep LISTEN | grep 9443
}

@test "Curl returns Hello, World" {
  curl -skL http://127.0.0.1:9080/ | grep "Hello, World"
}
