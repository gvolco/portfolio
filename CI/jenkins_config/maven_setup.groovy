#!groovy

// set up maven, script based on https://wiki.jenkins.io/display/JENKINS/Add+a+Maven+Installation%2C+Tool+Installation%2C+Modify+System+Config
import jenkins.*;
import jenkins.model.*;
import hudson.*;
import hudson.model.*;

mavenVersion = "3.5.0"
mavenName = "MAVEN3"

mavenPlugin=Jenkins.instance.getExtensionList(hudson.tasks.Maven.DescriptorImpl.class)[0];
mavenInstalls=(mavenPlugin.installations as List);
mavenInstalls.add(new hudson.tasks.Maven.MavenInstallation(mavenName, null, [new hudson.tools.InstallSourceProperty([new hudson.tasks.Maven.MavenInstaller(mavenVersion)])]));
mavenPlugin.installations=mavenInstalls
mavenPlugin.save()
