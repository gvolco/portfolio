#!/bin/bash

BASE_PATH="/mnt/efs/incoming"

SCRIPT_NAME=$(basename $0)
PIDS=`pidof -x $SCRIPT_NAME`
RUNNING_PROCS=$(echo $PIDS | wc -w)
LOG_DATE=$(date +'%F %T')

echo "$LOG_DATE Starting $SCRIPT_NAME"

if [[ "$RUNNING_PROCS" -gt 1 ]]; then
        echo "Already running with PID $PIDS"
        exit 1
fi

if [[ "$#" -ne 1 || ($1 != "daily" && $1 != "hourly") ]]; then
        echo -e "Usage:\n$SCRIPT_NAME daily\n$SCRIPT_NAME hourly\n"
        exit 1
fi

if [[ ! -d "$BASE_PATH" ]]; then
	echo "Directory $BASE_PATH does not exist. Is it mounted?"
	exit 1
fi

echo "Base path $BASE_PATH"
cd $BASE_PATH

if [[ "$1" = "hourly" ]]; then
  YEAR=$(date +'%Y')
  MONTH=$(date +'%m')
  DAY=$(date +'%d')
elif [[ "$1" = "daily" ]]; then
  YEAR=$(date +'%Y' -d '-1 day')
  MONTH=$(date +'%m' -d '-1 day')
  DAY=$(date +'%d' -d '-1 day')
else
  echo "Incorrect arguments"
  exit 1
fi

echo "Computed date ${YEAR}/${MONTH}/${DAY}"

LEVEL6_DIRS=$(find ./ -mindepth 6 -maxdepth 6 -type d)
for LEVEL6_DIR in $LEVEL6_DIRS
do
  TMP_DIR="${LEVEL6_DIR}/${YEAR}/${MONTH}/${DAY}"
  SYNC_DIR=$(echo "$TMP_DIR" | cut -c3-)
	if [ -d "$SYNC_DIR" ]; then
		/usr/local/bin/aws s3 sync /mnt/efs/incoming/${SYNC_DIR} s3://xxxxxxxxxxxxxxxxxxxxxx/incoming/${SYNC_DIR} --size-only
	fi
done

LOG_DATE=$(date +'%F %T')
echo "$LOG_DATE Completed $SCRIPT_NAME"
